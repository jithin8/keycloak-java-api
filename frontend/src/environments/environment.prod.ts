import { KeycloakConfig } from 'keycloak-angular';
const keycloakConfig: KeycloakConfig = {
  url: 'http://localhost:8181/auth',
  realm: 'microservicedb',
  clientId: 'angular-frontend'
};

export const environment = {
  production: true
};

