import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError, Subscriber } from 'rxjs';
import { catchError, delay } from 'rxjs/operators';
import { UserData, Attributes, PasswordData, Product } from '../models/UserModel';

@Injectable({
  providedIn: 'root'
})
export class DataService {


  baseUrl: string = "http://localhost:8181/auth/admin/realms/microservicedb/";

  keycloakLink: string = this.baseUrl + "users/";
  backendLink: string = "http://localhost:8085/api/";
  getEmployee: string = this.baseUrl + "roles/employee/users/"


  // base_url: string = "http://localhost:3000/api/users/";
  // _login_url: string = this.base_url + "login"
  constructor(private http: HttpClient, private router: Router) { }

  // loginApi(user): any {
  //   return this.http.post(this._login_url, user).pipe(catchError(this.errorHandler));
  // }

  getUsers(): Observable<UserData> {
    return this.http.get<UserData>(this.getEmployee);
  }

  addProduct(product: Product) {
    let link = this.backendLink + "product"
    return this.http.post(link, product);
  }

  getProducts(): Observable<Product> {
    let products = this.backendLink + "product"
    return this.http.get<Product>(products);
  }

  deleteProduct(product_id: any) {
    let link = this.backendLink + "product"
    let params = new HttpParams();
    params = params.append('_id', product_id);
    return this.http.delete(link + '/' + product_id);
  }

  getUserByUsername(username: string): Observable<UserData> {
    let params = new HttpParams();
    params = params.append('username', username);
    return this.http.get<UserData>(this.keycloakLink, { params: params });
  }

  getUserById(userId: string): Observable<UserData> {
    return this.http.get<UserData>(this.keycloakLink + userId);
  }

  // userData: UserData = new UserData();

  updateProfile(userData: UserData) {
    return this.http.put(this.keycloakLink + userData.id, userData);
  }

  updatePassword(userData: UserData, passwordData: PasswordData) {
    return this.http.put(this.keycloakLink + userData.id + "/reset-password", passwordData);
  }

  createUser(userData: UserData) {
    return this.http.post(this.keycloakLink, userData);
  }

  changeStatus(userid: string, currentStatus: boolean) {
    let userData = new UserData();
    userData.enabled = !currentStatus;
    return this.http.put(this.keycloakLink + userid, userData);
  }

  errorHandler(error: HttpErrorResponse) {
    console.log("Error handler")
    console.log(error.error.msg)
    return throwError(error.error.msg || "Server Error")
  }
}
