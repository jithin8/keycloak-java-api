import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavService {
  private navimage = new BehaviorSubject<string>(null);
  data = this.navimage.asObservable();
  constructor() { }

  updatenavimgurl(imgurl: string){
    this.navimage.next(imgurl);
  }
}
