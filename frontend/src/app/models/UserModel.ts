export class UserData {
    id: string;
    firstName: string;
    lastName: string;
    username: string;
    enabled: boolean;
    email: string;
    credentials: PasswordData[];
    attributes: Attributes
}

export class Attributes {
    avatar_url: string;
    mobile_number: string;
}


export class PasswordData {
    type: string;
    value: string;
    temporary: boolean;
}

export class Product {
    _id: string;
    name: string;
    price: number;
    description: string;

}

