import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { UserData, PasswordData } from 'src/app/models/UserModel';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  constructor(private dataService: DataService) { }

  ngOnInit() {
  }

  createGroup = new FormGroup({
    userName: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', [Validators.required])
  });

  status: boolean;
  msg: string = ''
  onSubmit() {
    let userData: UserData = new UserData();
    let passwordData: PasswordData = new PasswordData();

    passwordData.temporary = true;
    passwordData.type = "password";
    passwordData.value = this.createGroup.get("password").value;

    userData.username = this.createGroup.get("userName").value;
    userData.email = this.createGroup.get("email").value;
    userData.credentials = [passwordData];
    userData.enabled = true;
    this.dataService.createUser(userData).subscribe(res => {
      this.msg = "user created successfully!"
      this.status = true;
      this.createGroup.reset();
    }, err => {
      console.log("error create user");
      this.status = false;
      this.msg = err.error.errorMessage;
    });


  }

}
