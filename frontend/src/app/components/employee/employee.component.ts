import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { UserData, PasswordData } from 'src/app/models/UserModel';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor(private dataService: DataService, private keycloakService: KeycloakService) { }
  user: UserData;
  userDetails;
  username: string;
  userList;
  password = new PasswordData();

  async ngOnInit() {
    this.userDetails = await this.keycloakService.loadUserProfile();
    this.username = this.keycloakService.getUsername()

    this.dataService.getUserByUsername(this.username).subscribe(res => {
      this.userDetails = res[0];
    }
    );

    this.getUsers();
  }

  getUsers() {
    this.dataService.getUsers().subscribe(res => {
      this.userList = res;
      console.log(this.userList);
      console.log(this.userDetails);

    }, err => {
      console.log("error in list employee");
      console.log(err);
    });
  }

  changeStatus(userid: string, enabled: boolean) {
    this.dataService.changeStatus(userid, enabled).subscribe(res => {
      this.getUsers();
    }, err => {
      console.log(err);
    });
  }


}
