import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { faUserEdit } from '@fortawesome/free-solid-svg-icons';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  userEdit = faUserEdit;

  constructor(private keycloakService: KeycloakService, private dataService: DataService) {

  }
  userDetails: any;
  username: string;

  async ngOnInit() {
    if (await this.keycloakService.isLoggedIn()) {
      this.username = this.keycloakService.getUsername()
      this.dataService.getUserByUsername(this.username).subscribe(res => {
        this.userDetails = res[0];
      }
      );
    }
  }
}
