import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { UserData, Attributes } from 'src/app/models/UserModel';
import { Attribute } from '@angular/compiler';
import { KeycloakService } from 'keycloak-angular';
import { Router } from '@angular/router';
import { NavService } from 'src/app/services/nav.service';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.css']
})
export class ProfileEditComponent implements OnInit {

  constructor(private dataService: DataService, private keycloakService: KeycloakService, private router: Router, private navser: NavService) { }
  userDetails;
  profileGroup = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    email: new FormControl('', [Validators.email]),
    mobileNumber: new FormControl(''),
    avatarUrl: new FormControl('')
  });

  username;

  async ngOnInit() {
    this.userDetails = await this.keycloakService.loadUserProfile();


    this.username = this.keycloakService.getUsername()
    this.dataService.getUserByUsername(this.username).subscribe(res => {
      this.userDetails = res[0];
      this.updateValue(this.userDetails);
      console.log("userdetail");
      console.log(this.userDetails);
    }, err => {
      console.log("error");
      console.log(err);

    }
    );
  }

  updateValue(userDetails: any) {
    this.profileGroup.setValue({
      firstName: userDetails.firstName != null ? userDetails.firstName : '',
      lastName: userDetails.lastName != null ? userDetails.lastName : '',
      email: userDetails.email != null ? userDetails.email : '',
      mobileNumber: userDetails.attributes != null ? (userDetails.attributes.mobile_number != null ? userDetails.attributes.mobile_number : '') : '',
      avatarUrl: userDetails.attributes != null ? (userDetails.attributes.avatar_url != null ? userDetails.attributes.avatar_url : '') : ''
    });
  }

  userdata: UserData;

  onSubmit() {
    console.log("submit clicked");

    this.userdata = new UserData();
    this.userdata.id = this.userDetails.id;
    this.userdata.attributes = new Attributes();
    this.userdata.email = this.profileGroup.get("email").value;
    this.userdata.firstName = this.profileGroup.get("firstName").value;
    this.userdata.lastName = this.profileGroup.get("lastName").value;
    this.userdata.attributes.avatar_url = this.profileGroup.get("avatarUrl").value;
    this.userdata.attributes.mobile_number = this.profileGroup.get("mobileNumber").value;

    this.dataService.updateProfile(this.userdata).subscribe(res => {
      console.log("Profile updated Response");
      console.log(res);

      this.navser.updatenavimgurl(this.profileGroup.get("avatarUrl").value)

      this.router.navigate(['/profile']);
    },
      err => {
        console.log("Error");
        console.log(err);
      });
  }

}
