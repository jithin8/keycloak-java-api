import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/UserModel';
import { DataService } from 'src/app/services/data.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor(private dataService: DataService, private authService: AuthService) { }

  products;
  admin: boolean = false;

  productGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    price: new FormControl('', [Validators.required]),
    description: new FormControl('')
  });

  ngOnInit() {
    this.admin = this.authService.getRoles('admin');
    this.dataService.getProducts().subscribe((res) => {
      console.log(res);

      this.products = res;
    })
  }

  product: Product;
  onSubmit() {
    console.log("submit clicked");
    this.product = new Product();
    this.product.name = this.productGroup.get("name").value;
    this.product.price = this.productGroup.get("price").value;
    this.product.description = this.productGroup.get("description").value;

    this.dataService.addProduct(this.product).subscribe((res) => {
      console.log(res);
      this.products = res;
      this.productGroup.reset();
    })
  }

  deleteProduct(product_id) {
    this.dataService.deleteProduct(product_id).subscribe((res) => {
      console.log(res);
      this.products = res;
      this.productGroup.reset();
    }, (err) => {
      console.log(err);

    })
  }

}
