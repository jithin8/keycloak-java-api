import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PasswordData } from 'src/app/models/UserModel';
import { DataService } from 'src/app/services/data.service';
import { KeycloakService } from 'keycloak-angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  constructor(private dataService: DataService, private keycloakService: KeycloakService, private router: Router) { }
  userDetails;
  async ngOnInit() {

    this.userDetails = await this.keycloakService.loadUserProfile();
    let username = this.keycloakService.getUsername()
    this.dataService.getUserByUsername(username).subscribe(res => {
      this.userDetails = res[0];
    }
    );
  }

  passwordGroup = new FormGroup({
    newPassword: new FormControl('', [Validators.required])
  });

  password: PasswordData;
  msg = '';
  onSubmit() {
    this.password = new PasswordData();
    this.password.type = "password";
    this.password.value = this.passwordGroup.get("newPassword").value;

    this.dataService.updatePassword(this.userDetails, this.password).subscribe(res => {
      this.msg = "Password changed successfully"
      setTimeout(() => {
        this.router.navigateByUrl("/profile");
      }, 3000);
    }, err => {
      console.log("Error in password change");

    });
  }

}
