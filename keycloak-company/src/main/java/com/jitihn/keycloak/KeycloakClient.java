package com.jitihn.keycloak;

import com.jitihn.provider.CustomJacksonProvider;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;

public class KeycloakClient {

    private final String serverUrl;

    private final String realmId;

    private final String clientId;

    public KeycloakClient(final String serverUrl, final String realmId, final String clientId) {
        this.serverUrl = serverUrl;
        this.realmId = realmId;
        this.clientId = clientId;
    }

    // public Keycloak getKeycloak(String username, String password) {
    // KeycloakBuilder keycloak = getKeycloakBuilder(username, password);
    // return keycloak.build();
    // }

    public Keycloak getKeycloak(final String username, final String password) {
        // final KeycloakBuilder keycloak = getKeycloakBuilder(username, password);
        // return keycloak.build();
        return getKeycloakBuilder(username, password);
    }

    private Keycloak getKeycloakBuilder(final String username, final String password) {
        ResteasyClient resteasyClient = new ResteasyClientBuilder().connectionPoolSize(10)
                .register(new CustomJacksonProvider()).build();

        // ResteasyClient d ;

        // ClientHttpEngine httpEngine;
        // ExecutorService executorService;
        // ScheduledExecutorService scheduledExecutorService;
        // ClientConfiguration clientConfiguration;

        // final ClientHttpEngine httpEngine, final ExecutorService
        // asyncInvocationExecutor, final boolean cleanupExecutor,
        // final ScheduledExecutorService scheduledExecutorService, final
        // ClientConfiguration configuration

        // ResteasyImplProvider resteasyClient = new ResteasyImplProvider();

        return KeycloakBuilder.builder().serverUrl(this.serverUrl).realm(this.realmId).username(username)
                .password(password).clientId(this.clientId).resteasyClient(resteasyClient).build();

    }
}