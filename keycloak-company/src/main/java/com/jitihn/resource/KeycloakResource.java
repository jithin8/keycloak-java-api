package com.jitihn.resource;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.jitihn.service.KeycloakService;

@Path("/apis")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class KeycloakResource {

    @Inject
    KeycloakService service;

    @GET
    public Response get() {
        return Response.ok(service.getTest()).build();
    }
}