package com.jitihn;

import com.jitihn.keycloak.KeycloakClient;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;

public class Main {
    public static void main(String[] args) {
        String serverUrl = "http://localhost:8181/auth";
        String realmId = "master";
        String clientId = "admin-cli";
        KeycloakClient client = new KeycloakClient(serverUrl, realmId, clientId);
        Keycloak kc = client.getKeycloak("admin", "admin");
        RealmResource realmResource = kc.realm("keycloak-client");
        UsersResource userRessource = realmResource.users();
        if (userRessource != null) {
            System.out.println("Count: " + userRessource.count());
        } else {
            System.out.println("null");
        }

    }
}